import argparse
import time
import calendar
from datetime import datetime, timedelta
import concurrent.futures
import boto3
from boto3.dynamodb.conditions import Attr
import functools
from collections import Counter
import pickle
import pandas as pd


totalSegments = 16
dynamodb = boto3.resource('dynamodb', region_name='ap-northeast-1')


def scan_request(seg):
    list = []
    response = tbl.scan(TotalSegments=totalSegments, Segment=seg)

    list.append(response['Items'])
    while 'LastEvaluatedKey' in response:
        response = tbl.scan(TotalSegments=totalSegments, Segment=seg,
                            ExclusiveStartKey=response['LastEvaluatedKey'])
        list.append(response['Items'])

    items = functools.reduce(lambda x, y: x + y, list)
    print('Scanned #{}'.format(seg))
    return items

def scan_start():
    with concurrent.futures.ThreadPoolExecutor(max_workers=totalSegments) as pool:
        print("START")
        start = time.time()

        list1 = [items for items in pool.map(scan_request, range(totalSegments))]
        list2 = functools.reduce(lambda x, y: x + y, list1)
        print(len(list2))

        print("FINISHED")
        print('>> {} '.format(time.time() - start))
        return list2

def analysis(data_list):
    df = pd.DataFrame(data_list)
    df = df[df['rg'] == 'kr-01']
    print (len(df))
    df.cid = df.cid.map(lambda x: "c" + str(x))
    df['count'] = df['count'].apply(lambda x: Counter(x))
    df1 = df.groupby('cid')['count'].apply(lambda x: functools.reduce(lambda a, b: a + b, x)).unstack().fillna(0)
    most = df1.apply(lambda x: x.sort_values(ascending=False)[:5].sum(), axis=1)
    df1['total'] = df1.sum(axis=1)
    df1['most'] = most.map(lambda x: float(x))
    df2 = df1[(df1['total'] > 120) & (df1['most'] > df1['total'] * 0.95)]
    maybe_bots = pd.DataFrame(df2.index)
    bot_count = len(maybe_bots)
    total_count = len(df1)
    print("BOT COUNT: {}".format(bot_count))
    print("TOTAL COUNT: {}".format(total_count))
    print("BOT RATIO: {}".format(round(bot_count / total_count, 2)))
    dateStr = tableStr.split("-")[2]

    maybe_bots.to_csv("maybe_bots_" + dateStr + ".csv", index=None)
    print("Today's bot list is created.")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--date', help=('designate a date to scan bots')
    )
    args = parser.parse_args()
    tableStr = 'MabiHeroes-DmgCount-' + str(vars(args)['date'])
    print (tableStr)

    tbl = dynamodb.Table(tableStr)

    # DB scanning
    result = scan_start()

    # analysis
    analysis(result)

